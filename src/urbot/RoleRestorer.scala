// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import scala.collection.JavaConverters.asScalaBufferConverter
import scala.collection.mutable

import com.typesafe.scalalogging.Logger
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Role
import net.dv8tion.jda.core.entities.User
import urbot.JDBCExtensions.BindableStatement
import urbot.JDBCExtensions.ExtendedResultSet

private class RoleRestorer(connection: DatabaseConnection) extends PersistedModule(connection, "role-restorer") {
  override def enableIn(guild: Guild): Boolean = {
    if (!super.enableIn(guild)) {
      return false
    }
    val guildId = DiscordId(guild)
    logger.info("Reenabling role restorer in guild {}...", guildId)
    guild.getMembers.asScala.foreach(snapshotMember)
    logger.info("Reenabling role restorer in guild {} complete.", guildId)
    true
  }

  override def disableIn(guild: Guild): Boolean = {
    if (!super.disableIn(guild)) {
      return false
    }
    val guildId = DiscordId(guild)
    databaseConnection.withStatement("DELETE FROM persisted_user_roles WHERE guild_id=?") { stmt =>
      stmt.bind(guildId)
      stmt.executeUpdate()
    }
    databaseConnection.withStatement("DELETE FROM persisted_nicknames WHERE guild_id=?") { stmt =>
      stmt.bind(guildId)
      stmt.executeUpdate()
    }
    true
  }

  def getRestoredRolesAndNickname(member: Member): (mutable.Buffer[Role], Option[String]) = {
    val guild = member.getGuild
    val guildId = DiscordId(guild)
    val userId = DiscordId(member.getUser)
    logger.info("Restoring roles for user {} in guild {}", userId, guildId)
    val roles = getPersistedUserRoles(guild, userId)
    val nickname = getPersistedUserNickname(guildId, userId)
    (roles, nickname)
  }

  def snapshot(guild: Guild, tempTable: GuildRolesTemporaryTable): Unit = {
    val guildId = DiscordId(guild)
    logger.info("Updating snapshot of user roles and nicknames for guild {}...", guildId)
    guild.getMembers.asScala.foreach(snapshotMember)
    forgetNonexistingRoles(guildId, tempTable)
    logger.info("User role and nickname snapshot update for guild {} complete.", guildId)
  }

  def removeUser(guildId: DiscordId[Guild], userId: DiscordId[User]): Unit = {
    logger.info("Forgetting roles of user {} in guild {}", userId, guildId)
    forgetUserId(guildId, userId)
  }

  def addUserRoles(member: Member, addedRoles: Seq[Role]): Unit = {
    val guildId = DiscordId(member.getGuild)
    val userId = DiscordId(member.getUser)
    logger.info("Adding roles for user {} in guild {}: {}", userId, guildId, addedRoles)
    addUserRolesImpl(guildId, userId, addedRoles)
  }

  def removeUserRoles(member: Member, removedRoles: Seq[Role]): Unit = {
    val guildId = DiscordId(member.getGuild)
    val userId = DiscordId(member.getUser)
    logger.info("Removing roles for user {} in guild {}: {}", userId, guildId, removedRoles)
    removeUserRolesImpl(guildId, userId, removedRoles)
  }

  def forgetRole(guildId: DiscordId[Guild], roleId: DiscordId[Role]): Unit = {
    logger.info("Removing role {} from guild {}", roleId, guildId)
    databaseConnection.withStatement("DELETE FROM persisted_user_roles WHERE guild_id=? AND role_id=?") { stmt =>
      stmt.bind(guildId).bind(roleId)
      stmt.executeUpdate()
    }
  }

  def changeNickname(member: Member, newNickname: Option[String]): Unit = {
    val guildId = DiscordId(member.getGuild)
    val userId = DiscordId(member.getUser)
    logger.info("Updating nickname of member {} in guild {} to {}", userId, guildId, newNickname)
    changeNicknameImpl(guildId, userId, newNickname)
  }

  private def forgetNonexistingRoles(guildId: DiscordId[Guild], tempTable: GuildRolesTemporaryTable): Unit = {
    val deletedRows = databaseConnection.withStatement(
      s"""DELETE FROM persisted_user_roles
WHERE guild_id=? AND role_id NOT IN (SELECT role_id FROM ${tempTable.name})""") { stmt =>
      stmt.bind(guildId)
      stmt.executeUpdate()
    }
    logger.info("Pruned {} old roles from guild {}", deletedRows, guildId)
  }

  private def getPersistedUserRoles(guild: Guild, userId: DiscordId[User]): mutable.Buffer[Role] = {
    databaseConnection.withStatement(
      "SELECT role_id FROM persisted_user_roles WHERE guild_id=? AND user_id=?") { stmt =>
      stmt.bind(DiscordId(guild)).bind(userId)
      Utils.withResource(stmt.executeQuery()) { rs =>
        rs.iterate(x => Option(guild.getRoleById(x.getLong(1)))).flatten.toBuffer
      }
    }
  }

  private def getPersistedUserNickname(guildId: DiscordId[Guild], userId: DiscordId[User]): Option[String] = {
    databaseConnection.withStatement(
      "SELECT nickname FROM persisted_nicknames WHERE guild_id=? AND user_id=?") { stmt =>
      stmt.bind(guildId).bind(userId)
      Utils.withResource(stmt.executeQuery())(_.mapToOption(_.getString(1)))
    }
  }

  private def snapshotMember(member: Member): Unit = {
    val userId = DiscordId(member.getUser)
    val guildId = DiscordId(member.getGuild)
    forgetUserId(guildId, userId)
    addUserRolesImpl(guildId, userId, member.getRoles.asScala)
    changeNicknameImpl(guildId, userId, Option(member.getNickname))
  }

  private def forgetUserId(guildId: DiscordId[Guild], userId: DiscordId[User]): Unit = {
    databaseConnection.withStatement("DELETE FROM persisted_user_roles WHERE guild_id=? AND user_id=?") { stmt =>
      stmt.bind(guildId).bind(userId)
      stmt.executeUpdate()
    }
    changeNicknameImpl(guildId, userId, None)
  }

  private def changeNicknameImpl(guildId: DiscordId[Guild], userId: DiscordId[User], nickname: Option[String]): Unit = {
    nickname match {
      case None =>
        databaseConnection.withStatement("DELETE FROM persisted_nicknames WHERE guild_id=? AND user_id=?") { stmt =>
          stmt.bind(guildId).bind(userId)
          stmt.executeUpdate()
        }
      case Some(newNickname) =>
        databaseConnection.withStatement(
          "INSERT OR REPLACE INTO persisted_nicknames(guild_id, user_id, nickname) VALUES (?, ?, ?)") { stmt =>
          stmt.bind(guildId).bind(userId).bind(newNickname)
          stmt.executeUpdate()
        }
    }
  }

  private def addUserRolesImpl(guildId: DiscordId[Guild], userId: DiscordId[User], roles: Seq[Role]): Unit = {
    databaseConnection.withStatement(
      "INSERT OR IGNORE INTO persisted_user_roles(guild_id, user_id, role_id) VALUES (?, ?, ?)") { stmt =>
      roles.foreach { role =>
        stmt.bind(guildId).bind(userId).bind(DiscordId(role))
        stmt.executeUpdate()
      }
    }
  }

  private def removeUserRolesImpl(guildId: DiscordId[Guild], userId: DiscordId[User], roles: Seq[Role]): Unit = {
    databaseConnection.withStatement(
      "DELETE FROM persisted_user_roles WHERE guild_id=? AND user_id=? AND role_id=?") { stmt =>
      roles.foreach { role =>
        stmt.bind(guildId).bind(userId).bind(DiscordId(role))
        stmt.executeUpdate()
      }
    }
  }

  private val logger = Logger[RoleRestorer]
}
