// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.settings

import java.sql.PreparedStatement
import java.sql.ResultSet

import com.typesafe.scalalogging.Logger
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.Role
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.DatabaseConnection
import urbot.DiscordId
import urbot.JDBCExtensions.BindableStatement
import urbot.JDBCExtensions.ExtendedResultSet
import urbot.Utils
import urbot.commands.ParseResult
import urbot.commands.SettingCommand
import urbot.AlwaysEnabledModule

private[urbot] class GuildSettings(databaseConnection: DatabaseConnection) extends AlwaysEnabledModule {
  def apply[A](guildId: DiscordId[Guild], setting: GuildSetting[A]): Option[A] = {
    logger.debug("Retrieving guild setting {} in guild {}", setting.name, guildId)
    setting(guildId, this)
  }

  def update[A](guildId: DiscordId[Guild], setting: GuildSetting[A], newValue: A): Unit = {
    logger.info("Changing guild setting {} in guild {}", setting.name, guildId)
    setting(guildId, this) = newValue
  }

  def remove[A](guildId: DiscordId[Guild], setting: GuildSetting[A]): Unit = {
    val settingName = setting.name
    logger.info("Removing guild setting {} in guild {}", settingName, guildId)
    databaseConnection.withStatement("DELETE FROM guild_settings WHERE guild_id=? AND name=?") { stmt =>
      stmt.bind(guildId).bind(settingName)
      stmt.executeUpdate()
    }
  }

  private[settings] def getImpl[A](guildId: DiscordId[Guild], setting: GuildSetting[_],
    func: ResultSet => A): Option[A] = {
    databaseConnection.withStatement("SELECT value FROM guild_settings WHERE guild_id=? AND name=?") { stmt =>
      stmt.bind(guildId).bind(setting.name)
      Utils.withResource(stmt.executeQuery())(_.mapToOption(func(_)))
    }
  }

  private[settings] def setImpl[A](guildId: DiscordId[Guild], setting: GuildSetting[_],
    func: PreparedStatement => Unit): Unit = {
    databaseConnection.withStatement(
      "INSERT OR REPLACE INTO guild_settings(guild_id, name, value) VALUES(?, ?, ?)") { stmt =>
      stmt.bind(guildId).bind(setting.name)
      func(stmt)
      stmt.executeUpdate()
    }
  }

  private val logger = Logger[GuildSettings]
}

private[urbot] object ArrivalChannel extends TextChannelSetting {
  val name = "arrival-channel"
}

private[urbot] object Autorole extends RoleSetting {
  val name = "autorole"
}

private[urbot] object ArrivalMessage extends StringSetting {
  val name = "arrival-message"
}

private[urbot] object WelcomeBackMessage extends StringSetting {
  val name = "welcome-back-message"
}

private[urbot] object LeaveMessage extends StringSetting {
  val name = "leave-message"
}

private[urbot] object GuildSettings {
  private[settings] final val GET_COLUMN_INDEX: Int = 1
  private[settings] final val SET_COLUMN_INDEX: Int = 3

  val settingRegistry: Map[String, GuildSetting[_]] = {
    val seq = Seq[GuildSetting[_]](
      ArrivalChannel,
      Autorole,
      ArrivalMessage,
      WelcomeBackMessage,
      LeaveMessage)
    seq.map(s => (s.name, s)).toMap
  }
}

private[urbot] trait GuildSetting[A] {
  val name: String

  def apply(guildId: DiscordId[Guild], settings: GuildSettings): Option[A]

  def update(guildId: DiscordId[Guild], settings: GuildSettings, newValue: A): Unit

  def show(settings: GuildSettings, channel: TextChannel): RestAction[_]

  def parseSetting(settings: GuildSettings, message: Message, newValue: String): ParseResult
}

private[urbot] abstract class IdSetting[A] extends GuildSetting[DiscordId[A]] {
  final override def apply(guildId: DiscordId[Guild], settings: GuildSettings): Option[DiscordId[A]] = {
    settings.getImpl(guildId, this, _.getId(GuildSettings.GET_COLUMN_INDEX))
  }

  final override def update(guildId: DiscordId[Guild], settings: GuildSettings, newValue: DiscordId[A]): Unit = {
    settings.setImpl(guildId, this, _.setLong(GuildSettings.SET_COLUMN_INDEX, newValue.value))
  }
}

private[urbot] abstract class StringSetting extends GuildSetting[String] {
  final override def apply(guildId: DiscordId[Guild], settings: GuildSettings): Option[String] = {
    settings.getImpl(guildId, this, _.getString(GuildSettings.GET_COLUMN_INDEX))
  }

  final override def update(guildId: DiscordId[Guild], settings: GuildSettings, newValue: String): Unit = {
    settings.setImpl(guildId, this, _.setString(GuildSettings.SET_COLUMN_INDEX, newValue))
  }

  final override def show(settings: GuildSettings, channel: TextChannel): RestAction[_] = {
    SettingCommand.show(settings, channel, this)
  }

  final override def parseSetting(settings: GuildSettings, message: Message, rest: String): ParseResult = {
    SettingCommand.parseSetting(settings, message, rest, this)
  }
}

private[urbot] abstract class TextChannelSetting extends IdSetting[TextChannel] {
  final override def show(settings: GuildSettings, channel: TextChannel): RestAction[_] = {
    SettingCommand.show(settings, channel, this)
  }

  final override def parseSetting(settings: GuildSettings, message: Message, rest: String): ParseResult = {
    SettingCommand.parseSetting(settings, message, this)
  }
}

private[urbot] abstract class RoleSetting extends IdSetting[Role] {
  final override def show(settings: GuildSettings, channel: TextChannel): RestAction[_] = {
    SettingCommand.show(settings, channel, this)
  }

  final override def parseSetting(settings: GuildSettings, message: Message, rest: String): ParseResult = {
    SettingCommand.parseSetting(settings, message, this)
  }
}
