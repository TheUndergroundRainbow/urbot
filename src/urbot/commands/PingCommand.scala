// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction

private[commands] object PingCommand extends MemberFilter.Any with CommandModule.None {
  override def parse(_m: AssociatedModule, message: Message, extractor: FieldExtractor): ParseResult = {
    extractor.nextField() match {
      case Some(_) => ParseFailure("This command takes no arguments.")
      case None => Action
    }
  }

  private object Action extends CommandAction {
    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      Some(channel.sendMessage(authorMention + " pong."))
    }
  }

  override final val description =
    """Check if this bot is online.

Example of use:
 - `${COMMAND}`"""
}
