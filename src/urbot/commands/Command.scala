// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.Bot
import urbot.Module

private trait Command {
  type AssociatedModule <: Module

  def getAssociatedModule(bot: Bot): AssociatedModule

  def memberAllowed(member: Option[Member]): Boolean

  def parse(module: AssociatedModule, message: Message, extractor: FieldExtractor): ParseResult

  val description: String
}

private[urbot] sealed trait ParseResult

private final case class ParseFailure(message: String) extends ParseResult

private trait CommandAction extends ParseResult {
  def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]]
}
