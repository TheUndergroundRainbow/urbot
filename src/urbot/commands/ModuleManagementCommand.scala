// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.Bot
import urbot.PersistedModule

private[this] trait ModuleManagementCommand extends MemberFilter.SettingCapable with CommandModule.WholeBot {
  override final def parse(bot: Bot, message: Message, extractor: FieldExtractor): ParseResult = {
    val moduleName = extractor.nextField().getOrElse(return ParseFailure("Module name is required."))
    if (extractor.nextField().isDefined) {
      return ParseFailure("Only one module name allowed.")
    }
    val module = bot.moduleNameMap.getOrElse(moduleName, return ParseFailure(s"Unknown module name `${moduleName}`."))
    val guild = message.getGuild
    new CommandAction {
      override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
        act(module, guild)
        Some(channel.sendMessage(s"${authorMention}, module `${moduleName}` ${verb} successfully."))
      }
    }
  }

  protected def act(module: PersistedModule, guild: Guild): Unit

  protected val verb: String
}

private[commands] object ModuleManagementCommand {

  object Enable extends ModuleManagementCommand {
    override def act(module: PersistedModule, guild: Guild): Unit = module.enableIn(guild)

    override val verb = "enabled"

    override final val description =
      """Enable a module.

This command expects one argument `<module-name>`, being the name of the module to enable.
To get the module names, use the `list-modules` command.

Example of use:
 - `${COMMAND} message-log`: Enables the message log on this server."""
  }

  object Disable extends ModuleManagementCommand {
    override def act(module: PersistedModule, guild: Guild): Unit = module.disableIn(guild)

    override val verb = "disabled"

    override final val description =
      """Disable a module.

This command expects one argument `<module-name>`, being the name of the module to disable.
To get the module names, use the `list-modules` command.

Example of use:
 - `${COMMAND} message-log`: Disables the message log on this server."""
  }

}
