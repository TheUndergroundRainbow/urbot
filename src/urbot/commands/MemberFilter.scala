// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Member

private[commands] object MemberFilter {

  trait Any extends Command {
    override final def memberAllowed(member: Option[Member]): Boolean = true
  }

  trait ModOnly extends Command {
    override final def memberAllowed(member: Option[Member]): Boolean = {
      member.exists(m => modPermissions.exists(m.hasPermission(_)))
    }
  }

  trait SettingCapable extends Command {
    override final def memberAllowed(member: Option[Member]): Boolean = {
      member.exists(_.hasPermission(Permission.MANAGE_SERVER))
    }
  }

  trait ArchivalCapable extends Command {
    override final def memberAllowed(member: Option[Member]): Boolean = {
      member.exists(m => m.hasPermission(Permission.MANAGE_CHANNEL) || m.hasPermission(Permission.MESSAGE_MANAGE))
    }
  }

  trait RoleManagementCapable extends Command {
    override final def memberAllowed(member: Option[Member]): Boolean = {
      member.exists(_.hasPermission(Permission.MANAGE_ROLES))
    }
  }

  private[this] val modPermissions = Seq(
    Permission.BAN_MEMBERS,
    Permission.MANAGE_SERVER,
    Permission.MANAGE_CHANNEL,
    Permission.MANAGE_ROLES)
}
