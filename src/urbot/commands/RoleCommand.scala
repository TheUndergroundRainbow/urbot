// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.DiscordId
import urbot.RoleCategorizer

private[commands] object RoleCommand extends MemberFilter.Any with CommandModule.Roles {
  override def parse(categorizer: RoleCategorizer, message: Message, extractor: FieldExtractor): ParseResult = {
    val categoryName = extractor.nextField().getOrElse(return ParseFailure("Category name is required."))
    val roleName = extractor.getRest
    if (roleName.isEmpty) {
      return ParseFailure("Role name is required.")
    }
    new Action(categorizer, message.getMember, categoryName, roleName)
  }

  private final class Action(categorizer: RoleCategorizer, member: Member, categoryName: String, roleName: String)
    extends CommandAction {

    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      categorizer.findRoleIdInCategory(DiscordId(member.getGuild), categoryName, roleName) match {
        case None => Some(channel.sendMessage(s"${authorMention}, role `${roleName}` not found in `${categoryName}`."))
        case Some(roleId) =>
          Utils.toggleRole(channel, member, roleId).foreach(_.queue)
          None
      }
    }
  }

  override final val description =
    """Toggle a role from category for the calling user.

This command expects two arguments, `<role-category>` `<role-name>`, where `<category-name>` is the name of the
role category to search for the role in, and `<role-name>` is the role name (not mention!) to toggle.

The role is toggled, i.e. if the user already has that role, it will be removed, otherwise it will be added.

Normally this command is unnecessarily cumbersome to use, it serves only to disambiguate between roles of the same
name in different categories. Prefer using `r` instead.

Example of use:
 - `${COMMAND} foo Foo Level 5`: join or leave the role `Foo Level 5` found in category `foo` on this server."""
}
