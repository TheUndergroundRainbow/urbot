// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.DiscordId
import urbot.RoleCategorizer

private[commands] object RankCommand extends MemberFilter.Any with CommandModule.Roles {
  override def parse(categorizer: RoleCategorizer, message: Message, extractor: FieldExtractor): ParseResult = {
    val roleName = extractor.getRest
    if (roleName.isEmpty) {
      return ParseFailure("Role name is required.")
    }
    new Action(categorizer, message.getMember, roleName)
  }

  private final class Action(categorizer: RoleCategorizer, member: Member, roleName: String) extends CommandAction {
    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      categorizer.findRoleIdInGuild(DiscordId(member.getGuild), roleName) match {
        case Vector() =>
          Some(channel.sendMessage(s"${authorMention}, role `${roleName}` not found."))
        case Vector(roleId) =>
          Utils.toggleRole(channel, member, roleId).foreach(_.queue)
          None
        case _ =>
          Some(channel.sendMessage(
            s"${authorMention}, multiple roles named `${roleName}` found, use category search instead."))
      }
    }
  }

  override final val description =
    """Toggle a role for the calling user.

The command expects a single argument `<role-name>` being the role name to toggle.

The role is toggled, i.e. if the user already has that role, it will be removed, otherwise it will be added.

This command requires the role name to be unique server-wide. For dealing with ambiguous role names, prefer `role`
instead.

Example of use:
 - `${COMMAND} Color-Teal`: make the sending user join or leave the role Color-Teal."""
}
