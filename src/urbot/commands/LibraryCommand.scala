// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.DiscordId
import urbot.LibraryController

private[commands] object LibraryCommand extends MemberFilter.Any with CommandModule.Library {
  override def parse(library: LibraryController, message: Message, extractor: FieldExtractor): ParseResult = {
    val guildId = DiscordId(message.getGuild)
    val categoryName = extractor.nextField().getOrElse(return new ListCategoriesAction(library, guildId))
    val category = library(guildId, categoryName).getOrElse(
      return ParseFailure("Library category `" + categoryName + "` not found."))
    val topicName = extractor.nextField().getOrElse(return new ListTopicsAction(category))
    new ShowTopicAction(category, topicName)
  }

  private final class ListCategoriesAction(library: LibraryController, guildId: DiscordId[Guild])
    extends CommandAction {
    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      val categoryNames = library.listNames(guildId).mkString("\n")
      Some(channel.sendMessage("Available categories:```\n" + categoryNames + "```"))
    }
  }

  private final class ListTopicsAction(category: LibraryController#Category) extends CommandAction {
    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      val topicNames = category.listTopicNames().mkString("\n")
      Some(channel.sendMessage("Available topics:```\n" + topicNames + "```"))
    }
  }

  private final class ShowTopicAction(category: LibraryController#Category, topicName: String) extends CommandAction {
    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      category(topicName) match {
        case None => Some(channel.sendMessage(
          s"${authorMention}, library topic `${topicName}` not found in the given category."))
        case Some(content) => Some(channel.sendMessage(content))
      }
    }
  }

  override final val description =
    """View or list library topics.

With no arguments, list all library categories.
With a single argument `<category-name>`, list all topics in the given category.
With two arguments `<category-name> <topic-name>`, show the library entry of the given topic.

Example of use:
 - `${COMMAND}`: list all categories.
 - `${COMMAND} foo`: list all topics in category `foo`.
 - `${COMMAND} foo bar`: show the entry `bar` in category `foo`."""
}
