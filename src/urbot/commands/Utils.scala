// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import scala.collection.JavaConverters.asScalaBufferConverter

import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Role
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.DiscordId

private[commands] object Utils {
  def toggleRole(channel: TextChannel, member: Member, roleId: DiscordId[Role]): Seq[RestAction[_]] = {
    val guild = member.getGuild
    val controller = guild.getController
    val role = guild.getRoleById(roleId.value)
    if (!member.getRoles.asScala.exists(DiscordId(_) == roleId)) {
      val roleAction = controller.addSingleRoleToMember(member, role).reason("Requested via bot")
      Seq(roleAction, channel.sendMessage(s"${member.getAsMention}, you joined **${role.getName}**."))
    } else {
      val roleAction = controller.removeSingleRoleFromMember(member, role).reason("Requested via bot")
      Seq(roleAction, channel.sendMessage(s"${member.getAsMention}, you left **${role.getName}**."))
    }
  }
}
