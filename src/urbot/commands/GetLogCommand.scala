// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import java.nio.charset.StandardCharsets

import scala.util.Try

import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.DiscordId
import urbot.MessageLog
import urbot.MessageLogHTMLSerializer

private[commands] object GetLogCommand extends MemberFilter.ArchivalCapable with CommandModule.Log {
  override def parse(log: MessageLog, message: Message, extractor: FieldExtractor): ParseResult = {
    val countString = extractor.nextField().getOrElse(return ParseFailure("Message count is required."))
    val count = Try(countString.toInt).recover {
      case _ => return ParseFailure("`" + countString + "` doesn't seem to be a valid message count.")
    }.get
    if (count <= 0) {
      return ParseFailure("Message count must be a positive integer.")
    }
    val mentionedChannels = message.getMentionedChannels
    val channel = mentionedChannels.size match {
      case 0 => message.getTextChannel
      case 1 => mentionedChannels.get(0)
      case _ => return ParseFailure("Only one channel may be mentioned.")
    }
    if (message.getGuild.getTextChannelById(channel.getIdLong) == null) {
      return ParseFailure("The mentioned channel was not found on this server.")
    }
    new Action(log, channel, count)
  }

  private final class Action(log: MessageLog, channel: TextChannel, messageCount: Int) extends CommandAction {
    override def apply(outputChannel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      val logEntries = log.retrieve(DiscordId(channel.getGuild), DiscordId(channel), messageCount)
      val serializer = new MessageLogHTMLSerializer
      logEntries.foreach(serializer.serializeEvent)
      val bytes = serializer.getHTML.getBytes(StandardCharsets.UTF_8)
      Some(outputChannel.sendFile(bytes, "message-log.html"))
    }
  }

  override final val description =
    """Get messages from the message log.

This command accepts one or two arguments `<number-of-messages> (<channel-mention>)`.
If no channel is mentioned, the current channel is assumed.

The last `<number-of-messages>` messages in the mentioned channel are fetched from the log and uploaded as a file.
The log contains all actions, including edits and removals.

Example of use:
 - `${COMMAND} 100`: get the log of the last 100 messages in the current channel.
 - `${COMMAND} 200 #foo`: get the log of the last 200 messages in #foo."""
}
