// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.Bot
import urbot.DiscordId
import urbot.PersistedModule

private[commands] object ListModulesCommand extends MemberFilter.SettingCapable with CommandModule.WholeBot {
  override def parse(bot: Bot, message: Message, extractor: FieldExtractor): ParseResult = {
    extractor.nextField() match {
      case Some(_) => ParseFailure("This command takes no arguments.")
      case None => new Action(bot, DiscordId(message.getGuild))
    }
  }

  private final class Action(bot: Bot, guildId: DiscordId[Guild]) extends CommandAction {
    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      val builder = StringBuilder.newBuilder
      bot.moduleNameMap.values.foreach(formatModule(builder, _))
      Some(channel.sendMessage("```\n" + builder.toString + "```"))
    }

    private def formatModule(builder: StringBuilder, module: PersistedModule): Unit = {
      builder.append(if (module.isEnabledIn(guildId)) {
        '1'
      } else {
        '0'
      })
      builder.append(": ")
      builder.append(module.name)
      builder.append('\n')
    }
  }

  override final val description =
    """List bot modules and their status.

This command lists the bot modules and their current status: 0 for disabled modules, 1 for enabled.

Example of use:
 - `${COMMAND}`"""
}
