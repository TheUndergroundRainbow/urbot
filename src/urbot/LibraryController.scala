// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import com.typesafe.scalalogging.Logger
import net.dv8tion.jda.core.entities.Guild
import urbot.JDBCExtensions.BindableStatement
import urbot.JDBCExtensions.ExtendedResultSet

private class LibraryController(connection: DatabaseConnection) extends PersistedModule(connection, "library") {

  class Category(id: Long) {
    def apply(topicName: String): Option[String] = {
      databaseConnection.withStatement("SELECT content FROM library_topics WHERE category_id=? AND name=?") { stmt =>
        stmt.bind(id).bind(topicName)
        Utils.withResource(stmt.executeQuery())(_.mapToOption(_.getString(1)))
      }
    }

    def listTopicNames(): Seq[String] = {
      databaseConnection.withStatement("SELECT name FROM library_topics WHERE category_id=? ORDER BY name") { stmt =>
        stmt.bind(id)
        Utils.withResource(stmt.executeQuery())(_.iterate(_.getString(1)).toVector)
      }
    }

    def remove(topicName: String): Boolean = {
      logger.info("Removing entry `{}` from category {}", topicName, id)
      databaseConnection.withStatement("DELETE FROM library_topics WHERE category_id=? AND name=?") { stmt =>
        stmt.bind(id).bind(topicName)
        stmt.executeUpdate() > 0
      }
    }

    def update(topicName: String, newValue: String): Unit = {
      logger.info("Updating entry `{}` in category {}", topicName, id)
      databaseConnection.withStatement(
        "INSERT OR REPLACE INTO library_topics (category_id, name, content) VALUES (?, ?, ?)") { stmt =>
        stmt.bind(id).bind(topicName).bind(newValue)
        stmt.executeUpdate()
      }
    }
  }

  def add(guildId: DiscordId[Guild], categoryName: String): Unit = {
    logger.info("Adding category `{}` in guild {}", categoryName, guildId)
    databaseConnection.withStatement("INSERT INTO library_categories(guild_id, category_name) VALUES (?, ?)") { stmt =>
      stmt.bind(guildId).bind(categoryName)
      stmt.executeUpdate()
    }
  }

  def remove(guildId: DiscordId[Guild], categoryName: String): Boolean = {
    logger.info("Removing category `{}` in guild {}", categoryName, guildId)
    databaseConnection.withStatement("DELETE FROM library_categories WHERE guild_id=? AND category_name=?") { stmt =>
      stmt.bind(guildId).bind(categoryName)
      stmt.executeUpdate() > 0
    }
  }

  def listNames(guildId: DiscordId[Guild]): Seq[String] = {
    databaseConnection.withStatement(
      "SELECT category_name FROM library_categories WHERE guild_id=? ORDER BY category_name") { stmt =>
      stmt.bind(guildId)
      Utils.withResource(stmt.executeQuery())(_.iterate(_.getString(1)).toVector)
    }
  }

  def apply(guildId: DiscordId[Guild], categoryName: String): Option[Category] = {
    databaseConnection.withStatement(
      "SELECT category_id FROM library_categories WHERE guild_id=? AND category_name=?") { stmt =>
      stmt.bind(guildId).bind(categoryName)
      Utils.withResource(stmt.executeQuery())(_.mapToOption(rs => new Category(rs.getLong(1))))
    }
  }

  private val logger = Logger[LibraryController]
}
