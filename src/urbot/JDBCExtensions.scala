// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import java.sql.PreparedStatement
import java.sql.ResultSet

private[urbot] object JDBCExtensions {

  implicit final class BindableStatement(underlying: PreparedStatement) {
    def bind[A](value: A)(implicit binder: ParameterBinder[A]): BindableStatement = {
      currentParameterId += 1
      binder(underlying, currentParameterId, value)
      this
    }

    private var currentParameterId: Int = 0
  }

  implicit class ExtendedResultSet(private val underlying: ResultSet) extends AnyVal {
    def getId[A](columnIndex: Int): DiscordId[A] = new DiscordId[A](underlying.getLong(columnIndex))

    def getId[A](columnLabel: String): DiscordId[A] = new DiscordId[A](underlying.getLong(columnLabel))

    def mapToOption[A](func: ResultSet => A): Option[A] = {
      if (underlying.next()) {
        Some(func(underlying))
      } else {
        None
      }
    }

    def iterate[A](func: ResultSet => A): Iterator[A] = {
      val rs = underlying
      Iterator.continually((rs, rs.next())).takeWhile(_._2).map(t => func(t._1))
    }
  }

  trait ParameterBinder[-A] {
    def apply(statement: PreparedStatement, parameterId: Int, value: A): Unit
  }

  implicit object IdBinder extends ParameterBinder[DiscordId[Any]] {
    override def apply(statement: PreparedStatement, parameterId: Int, id: DiscordId[Any]): Unit = {
      statement.setLong(parameterId, id.value)
    }
  }

  implicit object IntBinder extends ParameterBinder[Int] {
    override def apply(statement: PreparedStatement, parameterId: Int, value: Int): Unit = {
      statement.setInt(parameterId, value)
    }
  }

  implicit object LongBinder extends ParameterBinder[Long] {
    override def apply(statement: PreparedStatement, parameterId: Int, value: Long): Unit = {
      statement.setLong(parameterId, value)
    }
  }

  implicit object StringBinder extends ParameterBinder[String] {
    override def apply(statement: PreparedStatement, parameterId: Int, value: String): Unit = {
      statement.setString(parameterId, value)
    }
  }

}
