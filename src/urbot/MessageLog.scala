// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import java.sql.ResultSet
import java.time.Instant

import scala.collection.JavaConverters.asScalaBufferConverter

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import urbot.JDBCExtensions.BindableStatement
import urbot.JDBCExtensions.ExtendedResultSet

private class MessageLog(connection: DatabaseConnection) extends PersistedModule(connection, "message-log") {
  def retrieve(guildId: DiscordId[Guild], channelId: DiscordId[TextChannel], msgCount: Int): Seq[MessageLog.Event] = {
    def makeEntry(rs: ResultSet): MessageLog.Event = MessageLog.Event(
      rs.getId[Message](1),
      MessageLog.EventType(rs.getInt(2)),
      Instant.ofEpochSecond(rs.getLong(3)),
      Option(rs.getString(4)))
    // NB: the event type needs to be interpolated into the query, otherwise
    // the message_creation index will not be picked. Since it's a source-level
    // thing, it's safe to do.
    databaseConnection.withStatement(
      s"""SELECT message_id, event_type, timestamp_secs, content FROM message_log
WHERE guild_id=? AND message_id IN (
  SELECT message_id FROM message_log WHERE guild_id=?1 AND channel_id=? AND event_type=${MessageLog.EventType.Create.id}
  ORDER BY message_id DESC LIMIT ?)
ORDER BY message_id ASC, event_id ASC""") { stmt =>
      stmt.bind(guildId).bind(channelId).bind(msgCount)
      Utils.withResource(stmt.executeQuery())(_.iterate(makeEntry).toVector)
    }
  }

  def logReceived(message: Message): Unit = {
    logEvent(
      MessageLog.EventType.Create,
      DiscordId(message.getGuild),
      DiscordId(message.getTextChannel),
      DiscordId(message),
      Instant.now(),
      MessageLog.serializeContent(message))
  }

  def logUpdated(message: Message): Unit = {
    logEvent(
      MessageLog.EventType.Update,
      DiscordId(message.getGuild),
      DiscordId(message.getTextChannel),
      DiscordId(message),
      Instant.now(),
      MessageLog.serializeContent(message))
  }

  def logRemoved(channel: TextChannel, messageId: DiscordId[Message]): Unit = {
    logEvent(
      MessageLog.EventType.Delete,
      DiscordId(channel.getGuild),
      DiscordId(channel),
      messageId,
      Instant.now(),
      null)
  }

  def logBulkRemoved(channel: TextChannel, messageIds: Seq[DiscordId[Message]]): Unit = {
    val now = Instant.now()
    messageIds.foreach { messageId =>
      logEvent(
        MessageLog.EventType.Delete,
        DiscordId(channel.getGuild),
        DiscordId(channel),
        messageId,
        now,
        null)
    }
  }

  override def disableIn(guild: Guild): Boolean = {
    if (!super.disableIn(guild)) {
      return false
    }
    databaseConnection.withStatement("DELETE FROM message_log WHERE guild_id=?") { stmt =>
      stmt.bind(DiscordId(guild))
      stmt.executeUpdate()
    }
    true
  }

  private def logEvent(eventType: MessageLog.EventType, guildId: DiscordId[Guild], channelId: DiscordId[TextChannel],
    messageId: DiscordId[Message], time: Instant, content: String): Unit = {
    databaseConnection.withStatement(
      """INSERT INTO message_log
(guild_id, channel_id, message_id, event_type, timestamp_secs, content) VALUES (?, ?, ?, ?, ?, ?)""") { stmt =>
      stmt.bind(guildId).bind(channelId).bind(messageId).bind(eventType.id).bind(time.getEpochSecond).bind(content)
      stmt.executeUpdate()
    }
  }
}

private object MessageLog {

  final case class Event(messageId: DiscordId[Message], eventType: EventType, timestamp: Instant,
    content: Option[String])

  object EventType {

    sealed abstract class Value(val id: Int)

    object Create extends Value(0)

    object Update extends Value(1)

    object Delete extends Value(2)

    def apply(value: Int): Value = {
      value match {
        case 0 => Create
        case 1 => Update
        case 2 => Delete
        case _ => throw new NoSuchElementException("Invalid message log entry type " + value.toString)
      }
    }
  }

  type EventType = EventType.Value

  private def serializeContent(message: Message): String = {
    val author = message.getAuthor
    val content = message.getContentDisplay
    val attachments = message.getAttachments.asScala.map(" <attachment: " + _.getUrl + ">").mkString("")
    author.getName + "#" + author.getDiscriminator + ": " + content + attachments
  }
}
