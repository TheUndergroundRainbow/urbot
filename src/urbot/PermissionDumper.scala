// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import scala.collection.JavaConverters.asScalaBufferConverter

import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.PermissionOverride
import net.dv8tion.jda.core.entities.Role
import net.dv8tion.jda.core.entities.TextChannel
import org.json.JSONArray
import org.json.JSONObject

private class PermissionDumper {
  def apply(guild: Guild): String = {
    dumpChannels(guild)
    dumpRoles(guild)
    dumpUsers(guild)
    array.toString(2)
  }

  private def dumpChannels(guild: Guild): Unit = {
    guild.getTextChannels.asScala.foreach { channel =>
      val channelJson = dumpObject(channel)
      channelJson.put("overrides", dumpChannelOverrides(channel))
      array.put(channelJson)
    }
  }

  private def dumpRoles(guild: Guild): Unit = {
    guild.getRoles.asScala.foreach { role =>
      val roleJson = dumpObject(role)
      roleJson.put("allowed", dumpPermissions(role.getPermissions.asScala))
      array.put(roleJson)
    }
  }

  private def dumpUsers(guild: Guild): Unit = {
    guild.getMembers.asScala.foreach { member =>
      val memberJson = dumpObject(member)
      memberJson.put("roles", dumpMemberRoles(member.getRoles.asScala))
      memberJson.put("permissions", dumpPermissions(member.getPermissions().asScala))
      array.put(memberJson)
    }
  }

  private def dumpMemberRoles(roles: Seq[Role]): JSONArray = {
    val result = new JSONArray
    roles.foreach { role => result.put(dumpObject(role)) }
    result
  }

  private def dumpChannelOverrides(channel: TextChannel): JSONArray = {
    val result = new JSONArray
    channel.getPermissionOverrides.asScala.foreach { permOverride =>
      val overrideJson = new JSONObject
      overrideJson.put("type", "permission override")
      overrideJson.put("owner", dumpOverrideOwner(permOverride))
      overrideJson.put("allowed", dumpPermissions(permOverride.getAllowed.asScala))
      overrideJson.put("denied", dumpPermissions(permOverride.getDenied.asScala))
      result.put(overrideJson)
    }
    result
  }

  private def dumpOverrideOwner(permOverride: PermissionOverride): JSONObject = {
    if (permOverride.isMemberOverride) {
      dumpObject(permOverride.getMember)
    } else {
      dumpObject(permOverride.getRole)
    }
  }

  private def dumpPermissions(perms: Seq[Permission]): JSONArray = {
    val result = new JSONArray
    perms.foreach { perm => result.put(perm.getName) }
    result
  }

  private def dumpObject(channel: TextChannel): JSONObject = {
    val result = new JSONObject
    result.put("type", "text channel")
    result.put("name", channel.getName)
    result.put("id", channel.getId)
    result
  }

  private def dumpObject(role: Role): JSONObject = {
    val result = new JSONObject
    result.put("type", "role")
    result.put("name", role.getName)
    result.put("id", role.getId)
    result
  }

  private def dumpObject(member: Member): JSONObject = {
    val result = new JSONObject
    result.put("type", "member")
    result.put("name", member.getEffectiveName)
    result.put("id", member.getUser.getId)
    result
  }

  private val array = new JSONArray
}
